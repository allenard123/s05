package com.zuitt;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    public void init() throws ServletException {
        System.out.println("Login has been initialized.");
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {

        HttpSession session = req.getSession();
        String firstname = session.getAttribute("firstname").toString();
        String lastname = session.getAttribute("lastname").toString();
        String job = session.getAttribute("job").toString();

        session.setAttribute("name", firstname + " " + lastname);
        session.setAttribute("job", job);

        res.sendRedirect("home.jsp");
    }

    public void destroy() {
        System.out.println("Login has been destroyed.");
    }
}
