package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;


@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

    public void init() throws ServletException {
        System.out.println("RegisterServlet has been initialized.");
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
        String firstname = req.getParameter("firstname");
        String lastname = req.getParameter("lastname");
        String phone = req.getParameter("phone");
        String email = req.getParameter("email");
        String app = req.getParameter("app");
        String birth_of_date = req.getParameter("birth_of_date");
        String job = req.getParameter("job");
        String description = req.getParameter("description");

        //Stores all the date from the form into the session
        HttpSession session = req.getSession();
        session.setAttribute("firstname", firstname);
        session.setAttribute("lastname", lastname);
        session.setAttribute("phone", phone);
        session.setAttribute("email", email);
        session.setAttribute("app", app);
        session.setAttribute("birth_of_date", birth_of_date);
        session.setAttribute("job", job);
        session.setAttribute("description", description);

        res.sendRedirect("register.jsp");
    }

    public void destroy() {
        System.out.println("RegisterServlet has been destroyed.");
    }
}