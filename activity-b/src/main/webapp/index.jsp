<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSP Creating Dynamic Content</title>

	<style>
		div {
			margin-top: 5px;
			margin-bottom: 5px;
		}
	</style>
	
</head>
<body>

	<h1>Welcome to Servlet Job Finder!</h1>
	
	<form method="post" action="register">
		<div>
			<label for="firstname">First Name</label>
			<input type="text" name="firstname" required>
		</div>
		
		<div>
			<label for="lastname">Last Name</label>
			<input type="text" name="lastname" required>
		</div>
		
		<div>
			<label for="phone">Phone</label>
			<input type="text" name="phone" required>
		</div>
		
		<div>
			<label for="email">Email</label>
			<input type="text" name="email" required>
		</div>
		
		<fieldset>
			<legend>How did you discover the app?</legend>
			<input type="radio" id="friends" name="app" required value="friends">
			<label for="friends">Friends</label>
			<br>
			
			<input type="radio" id="social_media" name="app" required value="social_media">
			<label for="social_media">Social Media</label>
			<br>
			
			<input type="radio" id="others" name="app" required value="others">
			<label for="others">Others</label>
			<br>		
		</fieldset>
		
		<div>
			<label for="birth_of_date">Date of birth</label>
			<input type="date" name="birth_of_date" required>
		</div>
		
		<div>
			<label for="job">Are you an employer or applicant?</label>
			<select id="hiring_position" name="job">
				<option value="applicant" selected="selected">Applicant</option>
				<option value="employer">Employer</option>
			</select>
		</div>
		
		<div>
			<label for="description">Profile Description</label>
			<textarea name="description" maxlength="500"></textarea>
		</div>
		
		<button>Register</button>
		
	</form>
	
</body>
</html>